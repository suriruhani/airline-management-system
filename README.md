# Airline Management System

12th Grade Final Project

The aim of this project is to develop a program to facilitate an Airline�s reservation process using Python programming language, version 2.7.4. 

This program can be divided into mainly two parts: one concerning the maintenance of the customer details and the other tracking the ticketing.

In the first part, the users are requested for their details such as name, password, e-mail id, bank account number, etc. and this is maintained as the instance of a class Customer and stored in a binary file. These details can be changed and the modifications will be updated and available for viewing via loading from the file.

The second part stores information about the flight ticket, such as starting city, destination, type of ticket, meal, etc. as instance of a derived class Ticket, inheriting from class Customer. A flat price is charged for reserving a seat and additional cost is added for different classes of tickets and types of cuisines. These are also stored in a binary file, and are available to be edited, subject to a fee (if applicable), and the difference will be charged to the bank account.

Lastly, this code also provides a facility to rate the airlines services and leave a comment. The former can be viewed as the average guest rating and the latter is stored in a text file, also available for viewing.
